package LabbUppgift2;

import java.util.NoSuchElementException;

abstract class Car {

    public Car() {
    }

    abstract void carSpeed();
    abstract void carAcceleration();
}

class Volvo extends Car {

    @Override
    void carSpeed() {
        System.out.println("Slow");
    }

    @Override
    void carAcceleration() {
        System.out.println("Slow");
    }
}

class Tesla extends Car {

    @Override
    void carSpeed() {
        System.out.println("Average Speed");
    }

    @Override
    void carAcceleration() {
        System.out.println("Super Fast Acceleration");
    }
}

class Bmw extends Car {

    @Override
    void carSpeed() {
        System.out.println("High Speed");
    }

    @Override
    void carAcceleration() {
        System.out.println("Average Acceleration");
    }
}
class Audi extends Car {

    @Override
    void carSpeed() {
        System.out.println("High Speed");
    }

    @Override
    void carAcceleration() {
        System.out.println("Fast Acceleration");
    }
}
class CarFactoryGermany extends CarFactory {

    @Override
    protected Car createCar(String germanType) {
        return switch (germanType.toLowerCase()) {
            case "bmw" -> new Bmw();
            case "tesla" -> new Tesla();
            case "audi" -> new Audi();
            default -> throw new NoSuchElementException("Den bilen existerar ej");
        };
    }
}
class CarFactorySweden extends CarFactory {

    @Override
    protected Car createCar(String sweType) {
        return switch (sweType.toLowerCase()) {
            case "volvo" -> new Volvo();
            case "tesla" -> new Tesla();
            default -> throw new NoSuchElementException("Den bilen existerar ej");
        };
    }
}

abstract class CarFactory {
    public Car orderCar(String carType) {
        Car car = createCar(carType);

        car.carAcceleration();
        car.carSpeed();

        return car;
    }
    protected abstract Car createCar(String carType);
}

public class Main {

    public static void main(String[] args) {

        CarFactory sweCars = new CarFactorySweden();
        CarFactory germanCars = new CarFactoryGermany();

        Car myAudi = germanCars.orderCar("audi");
        Car myVolvo = sweCars.orderCar("volvo");
    }
}
