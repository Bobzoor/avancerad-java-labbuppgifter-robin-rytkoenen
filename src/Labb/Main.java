package Labb;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


public class Main {

    public static void main(String[] args) {
        List<Person> persons = new ArrayList<>(List.of(
                new Person("Veroniqua", "female", 55000),
                new Person("Jonna", "female", 52000),
                new Person("Robin", "male", 19000),
                new Person("Kristofer", "male", 22000),
                new Person("Leya", "female", 83000),
                new Person("Jesper", "male", 25000),
                new Person("Ester", "female", 33000),
                new Person("Pär", "male", 67000),
                new Person("Jasmine", "female", 45000),
                new Person("Ragnar", "male", 39000)
        ));
        System.out.println("-----------------------------------------");
        // Snittlön Kvinnor
        double averagePayFemale = persons
                .stream()
                .filter(person -> person.getGender().equals("female"))
                .mapToDouble(Person::getSalary)
                .summaryStatistics()
                .getAverage();
        System.out.println("Average female salary: " + averagePayFemale);
        System.out.println("-----------------------------------------");

        // Snittlön Män
        double averagePayMale = persons
                .stream()
                .filter(person -> person.getGender().equals("male"))
                .mapToDouble(Person::getSalary)
                .summaryStatistics()
                .getAverage();
        System.out.println("Average male salary: " + averagePayMale);
        System.out.println("-----------------------------------------");

        // Snittlön Kvinnor och Män i grupp
        System.out.println(
                persons
                        .stream()
                        .collect(Collectors.groupingBy(
                                Person::getGender,
                                Collectors.averagingDouble(Person::getSalary)
                        ))
        );
        System.out.println("-----------------------------------------");

        // Lägst betalda person
        String lowestPaidPerson = persons
                .stream()
                .min(Comparator.comparing(Person::getSalary))
                .get()
                .getName();
        System.out.println("Lowest paid person is: " + lowestPaidPerson);
        System.out.println("-----------------------------------------");

        // Högst betalda person
        String highestPaidPerson = persons
                .stream()
                .max(Comparator.comparing(Person::getSalary))
                .get()
                .getName();
        System.out.println("Highest paid person is: " + highestPaidPerson);

    }
}
