package LabbUppgift4;

class PrimeCounter implements Runnable {
    int start;
    int end;

    public PrimeCounter(int start, int end) {
        this.start = start;
        this.end = end;
    }

    @Override
    public void run() {
        int count = 0;
        for(int i = start ; i < end ; i++)
            if(isPrime(i))
                count++;
        System.out.println("Antal primtal: " + count);


    }
    // Checking if a number is prime
    public static boolean isPrime(int x) {
        for(int i = 2 ; i * i <= x ; i++) {
            if(x % i == 0)
                return false;
        }
        return true;
    }
}

public class Main {

    public static void main(String[] args) throws InterruptedException {


        Thread thread1 = new Thread(new PrimeCounter(2,250000)); // Börjar på 2 då första primtalet är 2.
        Thread thread2 = new Thread(new PrimeCounter(250001,500000));

        Long start = System.nanoTime();
        thread1.start();
        thread1.join();
        thread2.start();
        thread2.join();
        Long end = System.nanoTime();
        System.out.println(end - start);


    }
}
