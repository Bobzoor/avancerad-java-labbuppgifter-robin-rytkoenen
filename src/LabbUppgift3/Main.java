package LabbUppgift3;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {

        // Skapa en lista av ord. Använd reguljära uttryck för plocka ut endast de ord som innehåller 2 eller fler engelska vokaler (a, e, i, o, u, y)

        List<String> words = new ArrayList<>(List.of(
                "Hero", "Programmer", "World of Warcraft", "Car", "Wood", "School", "Computer", "Dog", "House", "Bus"));

        Predicate<String> pattern = Pattern.compile("[aeiouAEIOUY][^ ]*[aeiouAEIOUY]").asPredicate();

        System.out.println(
            words
                        .stream()
                        .filter(pattern)
                        .collect(Collectors.toList())
        );

        Pattern pattern1 = Pattern.compile("[aeiouAEIOUY][^ ]*[aeiouAEIOUY]");

        List.of("Hero", "Programmer", "World of Warcraft", "Car", "Wood", "School", "Computer", "Dog", "House", "Bus")
                .stream()
                .filter(word -> pattern1.matcher(word).find())
                .forEach(System.out::println);

    }
}
